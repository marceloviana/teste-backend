<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Teste Back-End - Marcelo Viana</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <biblioteca-virtual livros='{{$livros}}'></biblioteca-virtual>
        </div>
    </body>
</html>
<script src="/js/app.js"></script>
