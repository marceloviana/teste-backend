## Teste de backend

Teste orientado à API. As requisições GET retornam um JSON de resposta, portanto, não encontrá interface grafica, execeto para o teste de API RESTful que usa interface gráfica construída com Vuejs para comunicar com backend utilizando verbos adequados para cada requisição.


Foi utilizado o Framework Laravel com propósito de otimizar a aplicação, organizar as atividades cada qual com sua responsabilidade e para aumentar a qualidade de entrega:

### Diretório principal do conteúdo codificado:
```
cd teste-backend/app/Http/Controllers
```
### Arquivos de rotas:
```
teste-backend/routes/web.php
teste-backend/routes/api.php
```

#### Requisitos para execução:
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Composer >= 1.9.1
- Nodejs >= v12.13.1
- npm >= 6.12.1
- Mysql >= 5.5 (Utilizado no CRUD de Biblioteca)
- Git

### Preparação:
```
git clone git@gitlab.com:marceloviana/teste-backend.git
cd teste-backend
composer install
npm install
php artisan migrate
```
Rodar o servidor
```
php artisan serve
# Retorna algo como http://localhost:8000/
```
### Resumindo a execução do projeto:
Você pode, ao invés de executar os passos acima, rodar o projeto no Docker. Deixei uma imagem pronta servindo a aplicação. Considerando que tenha o Docker instalado, basta rodar os comandos:

Baixar a imagem Docker:
```
docker pull registry.gitlab.com/marceloviana/teste-backend:latest
```
Executar:
```
docker run -dit -p 8000:80 registry.gitlab.com/marceloviana/teste-backend:latest
```
Acessar: http://localhost:8000/biblioteca

### Mais sobre a aplicação:
As atividades do teste foram organizadas em rotas, responde as requisições no formato JSON. As rotas disponíveis seguem abaixo:


- http://localhost:8000/arrays
- http://localhost:8000/recursiva
- http://localhost:8000/oo
- http://localhost:8000/biblioteca (interface gráfica em Vuejs para consumir a API REST)
- http://localhost:8000/api/
- http://localhost:8000/api/livro (Esta rota fornece aplicação no estilo de arquitetura REST, portanto utilize os verbos HTTP (POST, GET, PUT e DELETE) para interagir)
