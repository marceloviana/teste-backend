<?php

namespace App\Http\Controllers;

use App\Biblioteca;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class BibliotecaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $livros = Biblioteca::all();
      return view('biblioteca', compact('livros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $resposta = Biblioteca::create($request->all());
      return $resposta;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Biblioteca $biblioteca)
    {
        $livro = $request->all();
        $resposta = DB::table('biblioteca')
                                          ->where('id', $livro['id'])
                                          ->update([
                                            "titulo" => $livro['titulo'],
                                            "autor" => $livro['autor'],
                                            "edicao" => $livro['edicao']
                                          ]);
        return $resposta;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Biblioteca::destroy($id);
    }
}
