<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArrayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = array(['Mato Grosso', 'São Paulo', 'Rio de Janeiro', 'Minas Gerais', 'Espírito Santo'])[0];
        $siglas = array(['ES', 'MG', 'RJ', 'SP', 'MT'])[0];
        $estadoSigla = [];
        $verificadorEstado = [];

        foreach ($estados as $estado) {

          $primeiroNome = explode(' ', strtoupper($estado))[0];

          foreach ($siglas as $sigla) {
            $siglaArray = str_split($sigla);

            if (strstr($primeiroNome, strtoupper($siglaArray[0])) && strstr($primeiroNome, strtoupper($siglaArray[1]))) {
              $estadoSigla[] = [$sigla, $estado];
              $verificadorEstado[] = $estado;
              break;
            }
          }

          foreach ($siglas as $sigla) {
            $siglaArray = str_split($sigla);
            if (!in_array($estado, $verificadorEstado) && strstr($estado, strtoupper($siglaArray[0])) && strstr($estado, strtoupper($siglaArray[1]))) {
              $estadoSigla[] = [$sigla, $estado];
              $verificadorEstado[] = $estado;
            }
          }

        }

        return response()->json([
                          "mensagem" => "Hello Arrays",
                          "data" => $estadoSigla
                        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
