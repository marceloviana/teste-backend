<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrientacaoObjetoController extends Controller
{
    private $arroz = 0;
    private $feijao = 0;
    private $batata = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
                          "mensagem" => "Orientação a Objeto. Por favor, veja a implementação deste teste na Controller OrientacaoObjetoController.php",
                          "data" => 'Nada por aqui!. Experimente: http://'.$_SERVER['SERVER_NAME'].'/getset/5'
                        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($quantidade)
    {
        $this->arroz = $quantidade;
        $this->feijao = $quantidade;
        $this->batata = $quantidade;

        return ($this->arroz * $this->feijao * $this->batata);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($quantidade)
    {
      return response()->json([
                        "mensagem" => "Orientação a Objeto. Para chegar no resultado abaixo alguns métodos foram percorridos conforme enunciado. veja a implementação deste teste na Controller OrientacaoObjetoController.php",
                        "data" => 'Total de itens: '. $this->store($quantidade)
                      ]);
    }
}
