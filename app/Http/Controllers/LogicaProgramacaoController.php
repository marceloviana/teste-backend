<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogicaProgramacaoController extends Controller
{
    private $maximoInteriro = 1000;
    private $multiplos = array(3, 5);

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resultado = [];

        for ($i = 0; $i <= $this->maximoInteriro; $i++) {
          $multiplo = $this->multiplos[rand(0,1)];
          $resultado[] = (($i % $multiplo) === 0) ? $i.' é múltiplo de '.$multiplo: $i.' NÃO é múltiplo de '.$multiplo;
        }

        return response()->json([
                          "mensagem" => "Lógica de programação! Resultado dos múltiplos de 3 ou 5 (randomicamente):",
                          "data" => array_values($resultado)
                        ]);
    }
}
