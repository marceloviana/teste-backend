<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FuncaoRecursivaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($menorDivisivel = 30)
    {
        $numeroDivisor = [2, 3, 10];

        if (($menorDivisivel % $numeroDivisor[0] === 0) &&
            ($menorDivisivel % $numeroDivisor[1] === 0) &&
            ($menorDivisivel % $numeroDivisor[1] === 0)
        ) {
            return response()->json([
                "mensagem" => "Função recursiva. Este é o menor número divisível por 2, 3 e 10:",
                "data" => $menorDivisivel
               ]);
        }

        return response()->json([
            "mensagem" => "Função recursiva! $menorDivisivel não é o menor número divisível da seguência:",
            "data" => array_values($numeroDivisor)
          ]);
    }
}
