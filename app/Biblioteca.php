<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblioteca extends Model
{
  protected $fillable = [
      'titulo',
      'autor',
      'edicao'
  ];

  protected $table = 'biblioteca';
}
