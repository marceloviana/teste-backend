<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json([
        "mensagem" => "Olá! Você está na área de API. A única rota disponível é /api/livro. Utilize os Verbos do protocolo HTTP para interagir com os métodos."
      ]);
});
Route::resource('livro', 'BibliotecaController');
