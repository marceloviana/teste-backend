<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json([
        "mensagem" => "Olá! Este é meu teste de aplicação back-end. Aqui vai algumas instruções para acessar as rotas dos testes:",
        "teste_arrays" => "rota: /arrays",
        "funcao_recursiva" => "rota: /recursiva ou /recursiva/[algúm numero]",
        "orietancao_objeto" => "rota: /oo",
        "logica" => "rota: /logica",
        "biblioteca_interface" => "rota: /biblioteca",
        "biblioteca_api" => "rota: /api/ ou /api/livro/[verbo]",
        "nota" => "Todo teste foi baseado em API. Os retornos das requisições GET retornam um JSON de resposta, portanto, não encontrá interface grafica, execeto para o teste de API RESTful que usa interface gráfica cronstruída com Vuejs para comunicar com backend utilizando verbos adequados para cada requisição."
      ]);
});
Route::resource('arrays', 'ArrayController');
Route::get('recursiva/{divisor?}', 'FuncaoRecursivaController@index');
Route::resource('oo', 'OrientacaoObjetoController');
Route::resource('logica', 'LogicaProgramacaoController');
Route::resource('biblioteca', 'BibliotecaController');
